//
//  TableViewController.swift
//  AccessTwitterTimelineViaACAccount
//
//  Created by Chris Barker on 19/09/2014.
//  Copyright (c) 2014 CalicoWare. All rights reserved.
//

import UIKit
import Accounts
import Twitter
import Social

class TableViewController: UITableViewController {

    var accountStore = ACAccountStore()
    var setAccountType: ACAccountType = ACAccountStore().accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
    var twitterData = [NSDictionary]()
    
    var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
    
    @IBOutlet var tableViewController: UITableView!
    func GetTimeLine() {
        
        
        // Constant username value
        let screenName = "mrchrisbarker"
        
        // Parameters dictionary for addtional options (not covered in this tutorial)
        let params = Dictionary<String, String>()
        
        // Twitter API URL
        
        let url = NSURL(fileURLWithPath: "https://api.twitter.com/1.1/statuses/user_timeline.json?include_entities=true&include_rts=false&screen_name=\(screenName)&count=10")
//        let url = NSURL.URLWithString("https://api.twitter.com/1.1/statuses/user_timeline.json?include_entities=true&include_rts=false&screen_name=\(screenName)&count=10")
        
        
        // Create a new SLRequest
        var postRequest: SLRequest = SLRequest(forServiceType: SLServiceTypeTwitter, requestMethod: SLRequestMethod.GET, URL: url, parameters: params)
        
        let account: ACAccount = accountStore.accountsWithAccountType(setAccountType)[0] as ACAccount
        
        postRequest.account = account
        
        postRequest.performRequestWithHandler({responseData, urlResponse, error in
            
            // Variable to catch any errors thrown back by the Twitter API
            var error: NSError? = NSError()
            
            // Serialize JSON "responseData" into NSArray
            let feedData:NSArray? = NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.AllowFragments, error: &error) as? NSArray
            
            // Itterate and parse each tweet into NSDictionary
            for item: AnyObject in feedData! {
                

                // Cast 'item' into NSDictionary object
                var currentTweet:NSDictionary = item as NSDictionary
                
                self.twitterData.append(currentTweet as NSDictionary)
                
                // Access the 'user' key withing the 'currentTweet' object
                var currentUser:NSDictionary = currentTweet.objectForKey("user") as NSDictionary
                
                // Access the 'name' & 'text' from the 'currentUser' object
                var name = currentUser.objectForKey("name") as String
                var tweet = currentTweet.objectForKey("text") as String
                
                // Output result / add to object / add to UIListView
                println("Name: \(name), Tweet: \(tweet)")
                
            }
            
            self.tableViewController.reloadData()
            
        })
        
    }

    
    func GetPermission(){
        
        self.accountStore.requestAccessToAccountsWithType(setAccountType, options: nil) {
            granted, error in
            
            println("Permission Granted = \(granted)")
            
            if granted {
                self.GetTimeLine()
            }
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetPermission()
    }
    
    override func viewDidAppear(animated: Bool) {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
        actInd.startAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: (UITableView!)) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return twitterData.count
    }

    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
   
        var cell = tableView.dequeueReusableCellWithIdentifier("cellMain", forIndexPath: indexPath) as UITableViewCell
        
        // Configure the cell...
        var currentTweet: NSDictionary = twitterData[indexPath.section] as NSDictionary
        cell.textLabel.text = currentTweet.objectForKey("text") as? String
        
        actInd.stopAnimating()
        
        return cell
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellMain", forIndexPath: indexPath) as UITableViewCell
        
        var toDoItem:NSDictionary = toDoItems.objectAtIndex(indexPath.row) as NSDictionary
        cell.textLabel?.text = toDoItem.objectForKey("itemTitle") as? String
        
        
        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
